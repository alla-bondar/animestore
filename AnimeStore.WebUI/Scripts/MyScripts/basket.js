﻿/// <reference path="jquery-1.12.0.js" />

$(document).ready(function () {
    $("#deleteItem #deteteItemButton").click(
        function (event) {
            if (confirm("Удалить товар с корзины?")) {
                var currentBtn = $(this);
                var deleteItem = currentBtn.closest("#deleteItem");
                var orderItemId = deleteItem.find("#orderItemId").val();

                var currentOrderItemTr = currentBtn.closest("tr");

                deleteFromBasket(orderItemId, currentOrderItemTr);
            }
        })
});

function deleteFromBasket(orderItemId, currentOrderItemTr) {
    var data = {
        orderItemId: orderItemId
    };

    $.ajax({
        type: "POST",
        url: "/api/BasketApi/DeleteFromBasket",
        data: data,
        success: function (data) {
            console.log("success delete " + data);

           
            $("#basketItemsCount").text(data.Count);

            $("#totalPrice").text(data.TotalPrice);

            currentOrderItemTr.remove();
        }
    });
};

$(document).ready(function () {
    $(".confirmOrder #step3Button").click(
        function (event) {
            window.location.href = '/Cart/Step3';
        })
});

$(document).ready(function () {
    $(".confirmOrder #step4Button").click(
        function (event) {
            window.location.href = '/Cart/Step4';
        })
});

$(document).ready(function () {
    $("tr td #numOfComplect").change(
        function (event) {
            var currentNumberInput = $(this);
            var orderItemCount = currentNumberInput.val();
            var currentOrderItemTr = currentNumberInput.closest("tr");
            var orderItemId = currentOrderItemTr.find("#orderItemId").val();

            changeNumberOfComplects(orderItemId, orderItemCount, currentOrderItemTr);
        })
});

function changeNumberOfComplects(orderItemId, orderItemCount, currentOrderItemTr) {
    var data = {
        orderItemId: orderItemId,
        orderItemCount: orderItemCount
    };

    $.ajax({
        type: "POST",
        url: "/api/BasketApi/ChangeNumberOfComplects",
        data: data,
        success: function (data) {
            console.log("success changed " + data);

            $("#basketItemsCount").text(data.Count);

            currentOrderItemTr.find("#oneItemsPrice").text(data.OneItemsPrice);

            $("#totalPrice").text(data.TotalPrice);
        }
    });
};