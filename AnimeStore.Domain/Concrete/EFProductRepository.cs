﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;
using AnimeStore.Domain.Abstract;

namespace AnimeStore.Domain.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private readonly EFDbContext context;

        public EFProductRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Product> Products
        {
            get { return context.Products; }
        }

        public int SaveProduct(Product product)
        {
            if (product.ProductId == 0)
            {
                product.ProductId = context.Products.Add(product).ProductId;
            }
            else
            {
                Product dbEntry = context.Products.Find(product.ProductId);
                if (dbEntry != null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.Year = product.Year;
                    dbEntry.NumOfDiscsInComplect = product.NumOfDiscsInComplect;
                    dbEntry.Price = product.Price;
                    dbEntry.StockAvailability = product.StockAvailability;
                    dbEntry.NumOfSeries = product.NumOfSeries;
                    dbEntry.Director = product.Director;
                    dbEntry.Scriptwriter = product.Scriptwriter;
                    dbEntry.Description = product.Description;
                    dbEntry.Category = product.Category;
                    dbEntry.Image = product.Image;
                }
            }
            context.SaveChanges();
            return product.ProductId;
        }

        public Product DeleteProduct(int productId)
        {
            Product dbEntry = context.Products.Find(productId);
            if (dbEntry != null)
            {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
