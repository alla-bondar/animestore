﻿using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Controllers
{
    public class SearchController : Controller
    {
        private readonly ISearchService searchService;
        public const int PageSize = 3;

        public SearchController(ISearchService searchService)
        {
            this.searchService = searchService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult SearchRow()
        {
            return PartialView("SearchRow");
        }

        public ActionResult Search(string q, int page = 1)
        {
            var results = searchService.GetSearchResult(q);
            SearchResultsViewModel model = new SearchResultsViewModel
            {
                SearchResults = results
                                   .Skip((page - 1) * PageSize)
                                   .Take(PageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    TotalPages = (int)Math.Ceiling((double)results.Count / PageSize)
                },
                SearchKeyWord = q
            };

            return View("SearchResult", model);
        }
    }
}