﻿using System.Collections.Generic;

namespace AnimeStore.Domain.Entities
{
    public class Type
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Type()
        {
            Products = new HashSet<Product>();
        }
    }
}
