﻿using System;
using System.Collections.Generic;

namespace AnimeStore.Domain.Entities
{
    public class Order
    {
        public int OrderId { get; set; }

        public DateTime DateOfStartOrder { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public decimal TotalPrice { get; set; }

        public bool IsConfirmed { get; set; }

        public DateTime? DateOfOrder { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}
