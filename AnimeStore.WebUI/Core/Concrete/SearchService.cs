﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System.Collections.Generic;
using System.Linq;
using AnimeStore.WebUI.Infrastructure.Helpers;

namespace AnimeStore.WebUI.Core.Concrete
{
    public class SearchService : ISearchService
    {
        private readonly IProductRepository productRepository;
        private readonly INewsRepository newsRepository;

        public SearchService(IProductRepository productRepository, INewsRepository newsRepository)
        {
            this.productRepository = productRepository;
            this.newsRepository = newsRepository;
        }

        public List<SearchResultModel> GetSearchResult(string q)
        {
            List<SearchResultModel> searchResult = new List<SearchResultModel>();

            foreach (var product in productRepository.Products)
            {
                if (IsProductContain(product, q))
                {
                    SearchResultModel result = new SearchResultModel();
                    result.Name = product.Name;
                    result.Url = GetProductUrl(product.ProductId);
                    result.Preview = product.Description;
                    result.Image = product.Image.GetProductImageUrl();
                    searchResult.Add(result);
                }
            }

            foreach (var news in newsRepository.News)
            {
                if (IsNewsContain(news, q))
                {
                    SearchResultModel result = new SearchResultModel();
                    result.Name = news.Title;
                    result.Url = GetNewsUrl(news.NewsId);
                    result.Preview = news.Preview;
                    searchResult.Add(result);
                }
            }

            if (searchResult.Count == 0)
            {
                return searchResult;
            }

            return searchResult.OrderBy(s => s.Name).ToList();
        }

        private bool IsProductContain(Product product, string q)
        {
            if (product.Name.ContainsIgnoreCase(q) ||
                product.Description.ContainsIgnoreCase(q) ||
                product.Director.ContainsIgnoreCase(q) ||
                product.Scriptwriter.ContainsIgnoreCase(q))
            {
                return true;
            }

            return false;
        }

        private bool IsNewsContain(News news, string q)
        {
            if (news.Author.ContainsIgnoreCase(q) ||
                news.Category.ContainsIgnoreCase(q) ||
                news.Preview.ContainsIgnoreCase(q) ||
                news.Text.ContainsIgnoreCase(q) ||
                news.Title.ContainsIgnoreCase(q))
            {
                return true;
            }

            return false;
        }

        private string GetProductUrl(int productId)
        {
            return "/Catalogue/Info?id=" + productId;
        }

        private string GetNewsUrl(int newsId)
        {
            return "/News/Info?id=" + newsId;
        }
    }
}