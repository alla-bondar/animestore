﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Areas.Admin.Models;
using AnimeStore.WebUI.Infrastructure.Helpers;
using AnimeStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnimeStore.WebUI.Areas.Admin.Core.Abstract;

namespace AnimeStore.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ProductController : Controller
    {
        private readonly IProductRepository productRepository;
        private readonly ITypeRepository typeRepository;
        private readonly IGenreRepository genreRepository;
        private readonly IProductService productService;
        public const int PageSize = 10;

        public ProductController(IProductRepository productRepository,
            ITypeRepository typeRepository,
            IGenreRepository genreRepository,
            IProductService productService)
        {
            this.productRepository = productRepository;
            this.typeRepository = typeRepository;
            this.genreRepository = genreRepository;
            this.productService = productService;
        }

        public ViewResult Index(int page = 1)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = productRepository.Products
                    .OrderBy(game => game.ProductId)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    TotalPages = (int)Math.Ceiling((double)productRepository.Products.Count() / PageSize)
                }
            };
            return View(model);
        }

        public ViewResult Edit(int productId)
        {
            EditProductViewModel model = new EditProductViewModel();

            model.Product = productRepository.Products
                .FirstOrDefault(p => p.ProductId == productId);
            model.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ?
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            model.Types = GetTypesMultiSelectList();
            model.Genres = GetGenresMultiSelectList();

            if (model.Product != null)
            {
                model.TypeId = model.Product.Types.Select(t => t.TypeId).ToArray();
                model.GenreId = model.Product.Genres.Select(g => g.GenreId).ToArray();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditProductViewModel model, HttpPostedFileBase image = null)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    string fileName = System.IO.Path.GetFileName(image.FileName);

                    image.SaveAs(Server.MapPath(fileName.GetSavePathForProductImage()));
                    model.Product.Image = fileName;
                }
                model.Product.ProductId = productRepository.SaveProduct(model.Product);
                productService.SaveProductTypeAndGenre(model.Product.ProductId, model.TypeId, model.GenreId);

                TempData["message"] = $"Изменения в аниме \"{model.Product.Name}\" были сохранены";
                return Redirect(model.ReturnUrl);
            }

            model.Types = GetTypesMultiSelectList();
            model.Genres = GetGenresMultiSelectList();

            return View(model);
        }

        public ViewResult Create()
        {
            EditProductViewModel model = new EditProductViewModel();

            model.Product = new Product();
            model.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ?
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            model.Product.NumOfDiscsInComplect = 1;
            model.Product.NumOfSeries = 1;
            model.Product.Year = DateTime.Now.Year;

            model.Types = GetTypesMultiSelectList();
            model.Genres = GetGenresMultiSelectList();

            return View("Edit", model);
        }

        private MultiSelectList GetTypesMultiSelectList()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            foreach (var t in typeRepository.Types)
            {
                SelectListItem type = new SelectListItem
                {
                    Value = t.TypeId.ToString(),
                    Text = t.Name
                };
                types.Add(type);
            }

            return new MultiSelectList(types.OrderBy(i => i.Text), "Value", "Text");
        }

        private MultiSelectList GetGenresMultiSelectList()
        {
            List<SelectListItem> genres = new List<SelectListItem>();
            foreach (var g in genreRepository.Genres)
            {
                SelectListItem genre = new SelectListItem
                {
                    Value = g.GenreId.ToString(),
                    Text = g.Name
                };
                genres.Add(genre);
            }

            return new MultiSelectList(genres.OrderBy(i => i.Text), "Value", "Text");
        }

        [HttpPost]
        public ActionResult Delete(int productId)
        {
            productService.DeleteProductTypes(productId);
            productService.DeleteProductGenres(productId);

            Product deletedProduct = productRepository.DeleteProduct(productId);
            if (deletedProduct != null)
            {
                try
                {
                    if (deletedProduct.Image != null)
                    {
                        string path = Server.MapPath(deletedProduct.Image.GetSavePathForProductImage());
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                }
                catch (Exception)
                {
                    // ignored
                }

                TempData["message"] = $"Аниме \"{deletedProduct.Name}\" было удалено";
            }

            string returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ?
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            return Redirect(returnUrl);
        }
    }
}