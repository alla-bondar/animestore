﻿$(document).ready(function() {
    $("#selectTypes").multiselect({
        columns: 1,
        placeholder: "Выберите типы"
    });

    $("#selectGenres").multiselect({
        columns: 1,
        placeholder: "Выберите жанры"
    });
});
//jQuery MultiSelect Basic Uses:
//$("#selectTypes").multiselect({
//    columns: 1,
//    placeholder: "Выберите типы"
//});
//jQuery MultiSelect With Search Option:

    //$('#langOpt').multiselect({
    //    columns: 1,
    //    placeholder: 'Select Languages',
    //    search: true
    //});
//jQuery MultiSelect With Select All Option:

    //$('#langOpt').multiselect({
    //    columns: 1,
    //    placeholder: 'Select Languages',
    //    search: true,
    //    selectAll: true
    //});
//jQuery MultiSelect With Optgroup:

    //$('#langOptgroup').multiselect({
    //    columns: 4,
    //    placeholder: 'Select Languages',
    //    search: true,
    //    selectAll: true
    //});