﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.Domain.Abstract
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> Genres { get; }
    }
}
