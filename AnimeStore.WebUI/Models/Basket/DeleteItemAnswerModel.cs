﻿namespace AnimeStore.WebUI.Models.Basket
{
    public class DeleteItemAnswerModel
    {
        public int Count { get; set; }
        public decimal TotalPrice { get; set; }
    }
}