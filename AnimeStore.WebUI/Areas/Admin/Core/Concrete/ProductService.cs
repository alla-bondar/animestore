﻿using System;
using AnimeStore.WebUI.Areas.Admin.Core.Abstract;
using System.Linq;
using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;

namespace AnimeStore.WebUI.Areas.Admin.Core.Concrete
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;
        private readonly ITypeRepository typeRepository;
        private readonly IGenreRepository genreRepository;

        public ProductService(IProductRepository productRepository, ITypeRepository typeRepository,
            IGenreRepository genreRepository)
        {
            this.productRepository = productRepository;
            this.typeRepository = typeRepository;
            this.genreRepository = genreRepository;
        }

        public void SaveProductTypeAndGenre(int productId, int[] typeIds, int[] genreIds)
        {
            var product = GetProduct(productId);
            SaveProductTypes(product, typeIds);
            SaveProductGenres(product, genreIds);

            productRepository.SaveProduct(product);
        }

        private void SaveProductTypes(Product product, int[] typeIds)
        {
            var deletingTypes = product.Types.Where(el => !typeIds.Contains(el.TypeId)).ToList();

            foreach (var type in deletingTypes)
            {
                product.Types.Remove(type);
            }

            foreach (var type in typeIds)
            {
                if (product.Types.All(el => el.TypeId != type))
                {
                    product.Types.Add(typeRepository.Types.FirstOrDefault(t => t.TypeId == type));
                }
            }
        }

        private void SaveProductGenres(Product product, int[] genreIds)
        {
            var deletingGenres = product.Genres.Where(el => !genreIds.Contains(el.GenreId)).ToList();

            foreach (var genre in deletingGenres)
            {
                product.Genres.Remove(genre);
            }

            foreach (var genre in genreIds)
            {
                if (product.Genres.All(el => el.GenreId != genre))
                {
                    product.Genres.Add(genreRepository.Genres.FirstOrDefault(t => t.GenreId == genre));
                }
            }
        }

        public void DeleteProductTypes(int productId)
        {
            var product = GetProduct(productId);
            product.Types.Clear();
            productRepository.SaveProduct(product);
        }

        public void DeleteProductGenres(int productId)
        {
            var product = GetProduct(productId);
            product.Genres.Clear();
            productRepository.SaveProduct(product);
        }

        private Product GetProduct(int productId)
        {
            var product = productRepository.Products.FirstOrDefault(p => p.ProductId == productId);

            if (product == null)
            {
                throw new Exception("Product with id = " + productId + " doesn't exist.");
            }

            return product;
        }
    }
}