﻿using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Models.Basket;

namespace AnimeStore.WebUI.Core.Abstract
{
    public interface IBasketService
    {
        int AddToBasket(int productId, int count);
        DeleteItemAnswerModel DeleteFromBasket(int orderItemId);
        int GetOrderItemsCount();
        Order GetCurrentOrder();
        void ClearCart();
        ChangeNumberOfComplectsAnswerModel ChangeNumberOfComplects(ChangeNumberOfComplectsModel model);
        void ConfirmOrder();
    }
}
