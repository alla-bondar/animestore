﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System.Linq;
using System.Web.Security;
using WebMatrix.WebData;

namespace AnimeStore.WebUI.Core.Concrete
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public User GetCurrentUser()
        {
            return userRepository.Users.First(u => u.UserId == WebSecurity.CurrentUserId);
        }

        public bool IsUserWithLoginExists(string login)
        {
            return userRepository.Users.Any(u => u.Login.Equals(login));
        }

        public bool IsUserWithEmailExists(string email)
        {
            return userRepository.Users.Any(u => u.Email.Equals(email));
        }

        public CreateUserModel CreateUser(RegistrationModel model)
        {
            CreateUserModel createUserModel = new CreateUserModel();

            try
            {
                WebSecurity.CreateUserAndAccount(model.Login, model.Password,
                    new { FirstName = model.FirstName, LastName = model.LastName, Email = model.Email });
                createUserModel.IsSuccess = true;
            }
            catch (MembershipCreateUserException e)
            {
                createUserModel.ErrorMessage = e.StatusCode.ToString();
                createUserModel.IsSuccess = false;
            }

            return createUserModel;
        }

        public bool LoginUser(string login, string password, bool rememberMe = false)
        {
            return WebSecurity.Login(login, password, persistCookie: rememberMe);
        }

        public bool IsAuthenticated()
        {
            return WebSecurity.IsAuthenticated;
        }

        public void LogoutUser()
        {
            WebSecurity.Logout();
        }
    }
}