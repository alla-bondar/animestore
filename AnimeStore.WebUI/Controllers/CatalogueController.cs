﻿using System.Linq;
using System.Web.Mvc;
using AnimeStore.Domain.Abstract;
using AnimeStore.WebUI.Models;
using System;

namespace AnimeStore.WebUI.Controllers
{
    public class CatalogueController : Controller
    {
        private readonly IProductRepository productRepository;
        private const int PageSize = 4;

        public CatalogueController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public ActionResult Index(int page = 1)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = productRepository.Products
                    .OrderBy(game => game.ProductId)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    TotalPages = (int)Math.Ceiling((double)productRepository.Products.Count() / PageSize)
                }
            };
            return View(model);
        }

        public ActionResult Info(int id)
        {
            ChosenOrderItemModel model = new ChosenOrderItemModel();
            var result = productRepository.Products.FirstOrDefault(el => el.ProductId == id);
            model.Item = result;
            model.Count = 1;
            return View("Info", model);
        }

        public PartialViewResult InfoMini(int id)
        {
            ChosenOrderItemModel model = new ChosenOrderItemModel();
            var result = productRepository.Products.FirstOrDefault(el => el.ProductId == id);
            model.Item = result;
            model.Count = 1;
            return PartialView("InfoMini", model);
        }
    }
}
