﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.Domain.Abstract
{
    public interface ITypeRepository
    {
        IEnumerable<Type> Types { get; }
    }
}
