﻿using System;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace AnimeStore.WebUI.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ForNotAuthorize : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (WebSecurity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("/");
            }
        }
    }
}
