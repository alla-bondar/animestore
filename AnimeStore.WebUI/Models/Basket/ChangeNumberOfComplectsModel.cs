﻿namespace AnimeStore.WebUI.Models.Basket
{
    public class ChangeNumberOfComplectsModel
    {
        public int OrderItemId { get; set; }
        public int OrderItemCount { get; set; }
    }
}