﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.Domain.Concrete
{
    public class EFNewsRepository : INewsRepository
    {
        private readonly EFDbContext context;

        public EFNewsRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<News> News
        {
            get { return context.News; }
        }

        public News DeleteNews(int newsId)
        {
            News dbEntry = context.News.Find(newsId);
            if (dbEntry != null)
            {
                context.News.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public int SaveNews(News news)
        {
            if (news.NewsId == 0)
            {
                news.NewsId = context.News.Add(news).NewsId;
            }
            else
            {
                News dbEntry = context.News.Find(news.NewsId);
                if (dbEntry != null)
                {
                    dbEntry.UserId = news.UserId;
                    dbEntry.DateOfNews = news.DateOfNews;
                    dbEntry.Author = news.Author;
                    dbEntry.Title = news.Title;
                    dbEntry.Preview = news.Preview;
                    dbEntry.Text = news.Text;
                }
            }
            context.SaveChanges();
            return news.NewsId;
        }
    }
}
