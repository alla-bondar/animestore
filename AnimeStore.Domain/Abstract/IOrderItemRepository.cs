﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Abstract
{
    public interface IOrderItemRepository
    {
        IEnumerable<OrderItem> OrderItems { get; }

        void SaveOrderItem(OrderItem orderItem);

        void DeleteItem(int orderItemId);
    }
}
