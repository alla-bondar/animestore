﻿CREATE TABLE [dbo].[OrderItems] (
    [OrderItemId]      INT             IDENTITY (1, 1) NOT NULL,
    [OrderId]          INT             NOT NULL,
    [ProductId]        INT             NOT NULL,
    [NumOfComplects]   INT             DEFAULT ((1)) NOT NULL,
    [PriceForComplect] DECIMAL (16, 2) NOT NULL,
    PRIMARY KEY CLUSTERED ([OrderItemId] ASC), 
    CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY ([OrderId]) REFERENCES [Orders]([OrderId]), 
    CONSTRAINT [FK_OrderItems_Products] FOREIGN KEY ([ProductId]) REFERENCES [Products]([ProductId])
);

