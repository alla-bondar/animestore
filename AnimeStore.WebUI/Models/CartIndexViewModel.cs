﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.WebUI.Models
{
    public class CartIndexViewModel
    {
        public Order Order { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public User User { get; set; }
    }
}