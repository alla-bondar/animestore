﻿using System.Collections.Generic;

namespace AnimeStore.Domain.Entities
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Genre()
        {
            Products = new HashSet<Product>();
        }
    }
}
