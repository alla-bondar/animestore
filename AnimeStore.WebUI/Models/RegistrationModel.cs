﻿using System.ComponentModel.DataAnnotations;

namespace AnimeStore.WebUI.Models
{
    public class RegistrationModel
    {
        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Пожалуйста, введите логин")]
        [StringLength(20, ErrorMessage = "В логине должно быть от {2} до {1} символов", MinimumLength = 4)]
        public string Login { get; set; }

        [Display(Name = "Адрес электронной почты")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Пожалуйста, введите e-mail")]
        public string Email { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Пожалуйста, введите имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Пожалуйста, введите фамилию")]
        public string LastName { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Пожалуйста, введите пароль")]
        [StringLength(30, ErrorMessage = "В пароле должно быть от {2} до {1} символов", MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Повторите пароль")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Пожалуйста, повторите пароль")]
        [Compare("Password", ErrorMessage = "Пароль не совпадает")]
        public string ConfirmPassword { get; set; }
    }
}