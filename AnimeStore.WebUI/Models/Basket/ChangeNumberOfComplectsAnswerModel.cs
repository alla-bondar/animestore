﻿namespace AnimeStore.WebUI.Models.Basket
{
    public class ChangeNumberOfComplectsAnswerModel
    {
        public int Count { get; set; }
        public decimal OneItemsPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}