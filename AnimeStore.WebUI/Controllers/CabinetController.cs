﻿using AnimeStore.Domain.Abstract;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Controllers
{
    [Authorize]
    public class CabinetController : Controller
    {
        private readonly IOrderRepository orderRepository;
        private readonly IUserService userService;

        public CabinetController(IOrderRepository orderRepository, IUserService userService)
        {
            this.orderRepository = orderRepository;
            this.userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int days)
        {
            OrderHistoryModel ordersHistory = new OrderHistoryModel();
            ordersHistory.Days = days;
            List<OrderModel> orderList = new List<OrderModel>();
            int userId = userService.GetCurrentUser().UserId;
            var orders = orderRepository.Orders.Where(o => o.UserId == userId &&
                o.DateOfOrder >= DateTime.Today.AddDays(-days)).OrderByDescending(o => o.DateOfOrder);
            foreach (var o in orders)
            {
                OrderModel model = new OrderModel();
                model.Order = o;
                foreach (var oi in o.OrderItems)
                {
                    OrderItemInfoModel infoModel = new OrderItemInfoModel();
                    infoModel.OrderId = o.OrderId;
                    infoModel.Name = oi.Product.Name;
                    infoModel.NumDiscsInComplect = oi.Product.NumOfDiscsInComplect;
                    infoModel.NumOfComplects = oi.NumOfComplects;
                    infoModel.PriceForComplect = oi.PriceForComplect;
                    infoModel.TotalPrice = oi.NumOfComplects * oi.PriceForComplect;
                    model.OrderItemsInfo.Add(infoModel);
                }
                orderList.Add(model);
            }
            ordersHistory.Orders = orderList;
            return View(ordersHistory);
        }
    }
}
