﻿using System.Web.Http;
using AnimeStore.WebUI.Models.Basket;
using AnimeStore.WebUI.Core.Abstract;

namespace AnimeStore.WebUI.Controllers.Api
{
    public class BasketApiController : ApiController
    {
        private readonly IBasketService basketService;

        public BasketApiController(IBasketService basketService)
        {
            this.basketService = basketService;
        }

        [HttpPost]
        public int AddToBasket(AddToBasketModel model)
        {
            return basketService.AddToBasket(model.ProductId, model.Count);
        }

        [HttpPost]
        public DeleteItemAnswerModel DeleteFromBasket(DeleteFromBasketModel model)
        {
            return basketService.DeleteFromBasket(model.OrderItemId);
        }

        [HttpPost]
        public ChangeNumberOfComplectsAnswerModel ChangeNumberOfComplects(ChangeNumberOfComplectsModel model)
        {
            return basketService.ChangeNumberOfComplects(model);
        }
    }
}
