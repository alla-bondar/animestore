﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models.Basket;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Core.Concrete
{
    [Authorize]
    public class BasketService : IBasketService
    {
        private readonly IOrderRepository orderRepository;
        private readonly IProductRepository productRepository;
        private readonly IOrderItemRepository orderItemRepository;
        private readonly IUserService userService;

        public BasketService(IOrderRepository orderRepository,
            IProductRepository productRepository,
            IOrderItemRepository orderItemRepository,
            IUserService userService)
        {
            this.orderRepository = orderRepository;
            this.productRepository = productRepository;
            this.orderItemRepository = orderItemRepository;
            this.userService = userService;
        }

        public int AddToBasket(int productId, int count)
        {
            var order = GetCurrentOrder();

            var product = productRepository.Products.FirstOrDefault(el => el.ProductId == productId);
            if (product == null)
            {
                throw new System.Exception("Product with id " + productId + " is not found.");
            }

            var orderItem = order.OrderItems.FirstOrDefault(el => el.ProductId == productId);
            if (orderItem == null)
            {
                orderItem = new OrderItem();
                orderItem.OrderId = order.OrderId;
                orderItem.ProductId = product.ProductId;
                orderItem.PriceForComplect = product.Price;
            }

            orderItem.NumOfComplects += count;
            orderItemRepository.SaveOrderItem(orderItem);

            return order.OrderItems.Sum(el => el.NumOfComplects);
        }

        public int GetOrderItemsCount()
        {
            var order = GetCurrentOrder();
            return order.OrderItems.Sum(el => el.NumOfComplects);
        }

        public Order GetCurrentOrder()
        {
            var cartId = GetCartId();
            var order = orderRepository.Orders.First(el => el.OrderId == cartId);

            return order;
        }

        private int GetCartId()
        {
            var cartIdCookie = HttpContext.Current.Request.Cookies["CartId"];
            string cartId = cartIdCookie != null ? cartIdCookie.Value : string.Empty;
            int id;

            if (cartId == null || !int.TryParse(cartId, out id))
            {
                Order order = new Order();
                order.TotalPrice = 0;
                id = orderRepository.SaveOrder(order);
                HttpContext.Current.Response.Cookies["CartId"].Value = id.ToString();
            }

            return id;
        }

        public DeleteItemAnswerModel DeleteFromBasket(int orderItemId)
        {
            orderItemRepository.DeleteItem(orderItemId);
            Order order = GetCurrentOrder();

            DeleteItemAnswerModel answer = new DeleteItemAnswerModel();
            answer.Count = order.OrderItems.Sum(el => el.NumOfComplects);
            answer.TotalPrice = order.OrderItems.Sum(el => el.NumOfComplects * el.PriceForComplect);
            order.TotalPrice = answer.TotalPrice;
            orderRepository.SaveOrder(order);

            return answer;
        }

        public void ClearCart()
        {
            Order order = GetCurrentOrder();
            var orderItems = order.OrderItems.ToList();
            foreach (var item in orderItems)
            {
                orderItemRepository.DeleteItem(item.OrderItemId);
            }
            order.TotalPrice = 0;
        }

        private void DeleteCart()
        {
            var cartIdCookie = HttpContext.Current.Request.Cookies["CartId"];
            if (cartIdCookie != null)
            {
                HttpContext.Current.Response.Cookies["CartId"].Expires = System.DateTime.Now.AddDays(-1);
            }
        }

        public ChangeNumberOfComplectsAnswerModel ChangeNumberOfComplects(ChangeNumberOfComplectsModel model)
        {
            ChangeNumberOfComplectsAnswerModel answerModel = new ChangeNumberOfComplectsAnswerModel();

            Order order = GetCurrentOrder();
            OrderItem orderItem = order.OrderItems.First(oi => oi.OrderItemId == model.OrderItemId);
            orderItem.NumOfComplects = model.OrderItemCount;

            orderItemRepository.SaveOrderItem(orderItem);

            answerModel.OneItemsPrice = orderItem.PriceForComplect * orderItem.NumOfComplects;
            order.TotalPrice = answerModel.TotalPrice = order.OrderItems.Sum(oi => oi.PriceForComplect * oi.NumOfComplects);

            orderRepository.SaveOrder(order);

            answerModel.Count = order.OrderItems.Sum(oi => oi.NumOfComplects);

            return answerModel;
        }

        public void ConfirmOrder()
        {
            Order order = GetCurrentOrder();

            order.UserId = userService.GetCurrentUser().UserId;
            order.IsConfirmed = true;
            order.DateOfOrder = System.DateTime.Now;
            orderRepository.SaveOrder(order);

            DeleteCart();
        }
    }
}
