﻿namespace AnimeStore.WebUI.Entities
{
    public static class Constants
    {
        public const string ImagesPath = "/Content/image/";
        public const string ProductsImagesPath = "/Content/image/Products/";
        public const string PageMarker = "#";
    }
}