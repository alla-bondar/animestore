﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Controllers
{
    public class CartController : Controller
    {
        private readonly IOrderRepository orderRepository;
        private readonly IOrderItemRepository orderItemRepository;
        private readonly IUserService userService;
        private readonly IBasketService basketService;

        public CartController(IOrderRepository orderRepository,
            IOrderItemRepository orderItemRepository, IUserService userService,
            IBasketService basketService)
        {
            this.orderRepository = orderRepository;
            this.orderItemRepository = orderItemRepository;
            this.userService = userService;
            this.basketService = basketService;
        }

        public ActionResult Step1()
        {
            Order order = basketService.GetCurrentOrder();

            if (order.OrderItems.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            decimal totalPrice = 0;
            foreach (var oi in order.OrderItems)
            {
                oi.PriceForComplect = oi.Product.Price;
                totalPrice += oi.PriceForComplect * oi.NumOfComplects;
                orderItemRepository.SaveOrderItem(oi);
            }
            order.TotalPrice = totalPrice;

            orderRepository.SaveOrder(order);
            CartIndexViewModel cartModel = new CartIndexViewModel();
            cartModel.Order = order;
            cartModel.OrderItems = order.OrderItems.ToList();

            return View(cartModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Step2()
        {
            LoginOrRegistrationModel model = new LoginOrRegistrationModel();

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Step2(LoginOrRegistrationModel model)
        {

            if (model.LoginModel != null)
            {
                if (ModelState.IsValid && userService.LoginUser(model.LoginModel.Login, 
                    model.LoginModel.Password, model.LoginModel.RememberMe))
                {
                    return RedirectToAction("Step3");
                }

                ModelState.AddModelError("modelError",
                            "Неверный логин или пароль");
            }
            else
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                } 

                if (userService.IsUserWithLoginExists(model.RegistrationModel.Login))
                {
                    ModelState.AddModelError("NotUniqueUserError",
                        "Пользователь с таким логином уже зарегистрирован!");
                }
                Regex regex = new Regex("^[a-zA-Z0-9]*$");
                if (!regex.IsMatch(model.RegistrationModel.Login))
                {
                    ModelState.AddModelError("RegistrationModel.Login",
                        "Логин должен состоять только с цифр и латинских букв");
                }
                if (userService.IsUserWithEmailExists(model.RegistrationModel.Email))
                {
                    ModelState.AddModelError("NotUniqueUserError",
                        "Пользователь с таким email уже зарегистрирован!");
                }
                regex = new Regex("[0-9]+");
                if (regex.IsMatch(model.RegistrationModel.FirstName))
                {
                    ModelState.AddModelError("RegistrationModel.FirstName",
                        "В имени не должно быть цифр");
                }
                if (regex.IsMatch(model.RegistrationModel.LastName))
                {
                    ModelState.AddModelError("RegistrationModel.LastName",
                        "В фамилии не должно быть цифр");
                }
                if (ModelState.IsValid)
                {
                    CreateUserModel createUserModel = userService.CreateUser(model.RegistrationModel);
                    if (createUserModel.IsSuccess)
                    {
                        userService.LoginUser(model.RegistrationModel.Login, model.RegistrationModel.Password);
                        return RedirectToAction("Step3");
                    }

                    ModelState.AddModelError("NotUniqueUserError", createUserModel.ErrorMessage);
                }
            }

            return View(model);
        }

        public ActionResult Step3()
        {
            if (!userService.IsAuthenticated())
            {
                return RedirectToAction("Step2");
            }

            Order order = basketService.GetCurrentOrder();

            if (order.OrderItems.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            CartIndexViewModel cartModel = new CartIndexViewModel();
            cartModel.Order = order;
            cartModel.OrderItems = order.OrderItems.ToList();
            cartModel.User = userService.GetCurrentUser();

            return View(cartModel);
        }

        public ActionResult Step4()
        {
            if (!userService.IsAuthenticated())
            {
                return RedirectToAction("Step2");
            }

            Order order = basketService.GetCurrentOrder();

            if (order.OrderItems.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            basketService.ConfirmOrder();

            return View();
        }

        public PartialViewResult Basket()
        {
            string count = basketService.GetOrderItemsCount().ToString();
            return PartialView("Basket", count);
        }
    }
}
