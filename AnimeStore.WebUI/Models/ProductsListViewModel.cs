﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.WebUI.Models
{
    public class ProductsListViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}