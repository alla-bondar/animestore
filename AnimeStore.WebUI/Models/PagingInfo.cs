﻿namespace AnimeStore.WebUI.Models
{
    public class PagingInfo
    {
        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }
    }
}