﻿using System.Collections.Generic;
using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Concrete
{
    public class EFGenreRepository : IGenreRepository
    {
        private readonly EFDbContext context;

        public EFGenreRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Genre> Genres
        {
            get { return context.Genres; }
        }
    }
}
