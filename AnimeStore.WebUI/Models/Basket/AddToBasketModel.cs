﻿namespace AnimeStore.WebUI.Models.Basket
{
    public class AddToBasketModel
    {
        public int ProductId { get; set; }
        public int Count { get; set; }
    }
}
