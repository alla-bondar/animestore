﻿namespace AnimeStore.WebUI.Models
{
    public class LoginOrRegistrationModel
    {
        public LoginModel LoginModel { get; set; }
        public RegistrationModel RegistrationModel { get; set; }
    }
}