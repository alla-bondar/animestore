﻿using AnimeStore.WebUI.Models;
using System.Collections.Generic;

namespace AnimeStore.WebUI.Core.Abstract
{
    public interface ISearchService
    {
        List<SearchResultModel> GetSearchResult(string q);
    }
}
