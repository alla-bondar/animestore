﻿CREATE TABLE [dbo].[Orders] (
    [OrderId]          INT             IDENTITY (1, 1) NOT NULL,
    [DateOfStartOrder] DATETIME        DEFAULT (getdate()) NOT NULL,
    [UserId]           INT             NOT NULL,
    [TotalPrice]       DECIMAL (16, 2) NOT NULL,
    [IsConfirmed]      BIT             DEFAULT ((0)) NOT NULL,
    [DateOfOrder]      DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([OrderId] ASC), 
    CONSTRAINT [FK_Orders_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([UserId])
);

