﻿namespace AnimeStore.WebUI.Models
{
    public class OrderItemInfoModel
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public int NumDiscsInComplect { get; set; }
        public int NumOfComplects { get; set; }
        public decimal PriceForComplect { get; set; }
        public decimal TotalPrice { get; set; }
    }
}