﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.Domain.Abstract
{
    public interface INewsRepository
    {
        IEnumerable<News> News { get; }

        int SaveNews(News news);

        News DeleteNews(int newsId);
    }
}
