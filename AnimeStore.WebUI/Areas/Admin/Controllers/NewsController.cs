﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Areas.Admin.Models;
using AnimeStore.WebUI.Core.Abstract;
using AnimeStore.WebUI.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class EditNewsController : Controller
    {
        private readonly INewsRepository newsRepository;
        private readonly IUserService userService;
        public const int PageSize = 3;

        public EditNewsController(INewsRepository newsRepository, IUserService userService)
        {
            this.newsRepository = newsRepository;
            this.userService = userService;
        }

        public ActionResult Index(int page = 1)
        {
            NewsListViewModel model = new NewsListViewModel
            {
                News = newsRepository.News
                                .OrderBy(news => news.NewsId)
                                .Skip((page - 1) * PageSize)
                                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    TotalPages = (int)Math.Ceiling((double)newsRepository.News.Count() / PageSize)
                }
            };
            return View(model);
        }

        public ViewResult Edit(int newsId)
        {
            EditNewsViewModel model = new EditNewsViewModel();

            model.News = newsRepository.News
                .FirstOrDefault(p => p.NewsId == newsId);
            model.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ? 
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditNewsViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.News.DateOfNews = DateTime.Now;
                model.News.UserId = userService.GetCurrentUser().UserId;
                newsRepository.SaveNews(model.News);
                TempData["message"] = $"Изменения в новости \"{model.News.Title}\" были сохранены";
                return Redirect(model.ReturnUrl);
            }

            return View(model);
        }

        public ViewResult Create()
        {
            EditNewsViewModel model = new EditNewsViewModel();

            model.News = new News();
            model.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ?
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Delete(int newsId)
        {
            News deletedNews = newsRepository.DeleteNews(newsId);
            if (deletedNews != null)
            {
                TempData["message"] = $"Новость \"{deletedNews.Title}\" была удалена";
            }

            string returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ?
                System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            return Redirect(returnUrl);
        }
    }
}