﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Abstract
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }

        int SaveProduct(Product product);

        Product DeleteProduct(int productId);
    }
}
