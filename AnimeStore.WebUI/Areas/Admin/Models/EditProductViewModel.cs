﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Areas.Admin.Models
{
    public class EditProductViewModel
    {
        public Product Product { get; set; }
        public int[] TypeId { get; set; }
        public MultiSelectList Types { get; set; }
        public int[] GenreId { get; set; }
        public MultiSelectList Genres { get; set; }
        public string ReturnUrl { get; set; }
    }
}