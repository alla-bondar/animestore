﻿using AnimeStore.Domain.Entities;

namespace AnimeStore.WebUI.Areas.Admin.Models
{
    public class EditNewsViewModel
    {
        public News News { get; set; }
        public string ReturnUrl { get; set; }
    }
}