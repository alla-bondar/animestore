/// <reference path="jquery-1.12.0.js" />

$(document).ready(function () {
    $("#productInfoBlock #addToCart").click(
        function (event) {
            var currentBtn = $(this);
            var productInfoBlock = currentBtn.closest("#productInfoBlock");
            var productId = productInfoBlock.find("#productId").val();
            var count = productInfoBlock.find("#numOfComplect").val();

            addToBasket(productId,count);
        })
});

function addToBasket(productId, count) {
    var data = {
        productId: productId,
        count: count
    };

    $.ajax({
        type: "POST",
        url: "/api/BasketApi/AddToBasket",
        data: data,
        success: function (data) {
            console.log("success " + data);

            $("#basketItemsCount").text(data);
        }
    });
};