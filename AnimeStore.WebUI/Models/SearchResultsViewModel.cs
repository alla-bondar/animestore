﻿using System.Collections.Generic;

namespace AnimeStore.WebUI.Models
{
    public class SearchResultsViewModel
    {
        public List<SearchResultModel> SearchResults { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string SearchKeyWord { get; set; }
    }
}