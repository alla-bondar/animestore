﻿$(document).ready(function () {
    $("#searchDiv #searchButton").click(
        function(event) {
            var currentBtn = $(this);
            var searchDiv = currentBtn.closest("#searchDiv");
            var searchString = searchDiv.find("#searchString").val();

            SearchByKeyword(searchString);
        });

    $("#searchDiv #searchString").keydown(function (e) {
        if (e.keyCode === 13) {
            var searchString = $(this).val();

            SearchByKeyword(searchString);
        }
    });
});

function SearchByKeyword(keyword) {
    window.location.href = '/Search/Search?q=' + keyword;
};