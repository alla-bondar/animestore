﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AnimeStore.Domain.Entities
{
    public class Product
    {
        [HiddenInput(DisplayValue = false)]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите название аниме")]
        [Display(Name = "Название")]
        public string Name { get; set; }


        [Display(Name = "Год")]
        [Required(ErrorMessage = "Пожалуйста, введите год")]
        [Range(1900, int.MaxValue, ErrorMessage = "Пожалуйста, введите год")]
        public int Year { get; set; }

        [Display(Name = "Кол-во дисков в комплекте")]
        [Required(ErrorMessage = "Пожалуйста, укажите количество дисков в комплекте")]
        [Range(1, int.MaxValue, ErrorMessage = "Пожалуйста, введите положительное значение для количества дисков в комплекте")]
        public int NumOfDiscsInComplect { get; set; }

        [Display(Name = "Цена (грн)")]
        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage = "Пожалуйста, введите положительное значение для цены")]
        public decimal Price { get; set; }

        [Display(Name = "Наличие на складе")]
        public bool StockAvailability { get; set; }

        [Display(Name = "Кол-во серий")]
        [Required(ErrorMessage = "Пожалуйста, укажите количество серий")]
        [Range(1, int.MaxValue, ErrorMessage = "Пожалуйста, введите положительное значение для количества серий")]
        public int NumOfSeries { get; set; }

        [Display(Name = "Режисер")]
        [Required(ErrorMessage = "Пожалуйста, укажите режисера")]
        public string Director { get; set; }

        [Display(Name = "Сценарист")]
        [Required(ErrorMessage = "Пожалуйста, укажите сценариста")]
        public string Scriptwriter { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Пожалуйста, введите описание для аниме")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Пожалуйста, укажите категорию для аниме")]
        [Display(Name = "Категория")]
        [StringLength(1, ErrorMessage = "В категории должен быть 1 символ", MinimumLength = 1)]
        public string Category { get; set; }

        [Display(Name = "Обложка")]
        public string Image { get; set; }

        public virtual ICollection<Type> Types { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }

        public Product()
        {
            Types = new HashSet<Type>();
            Genres = new HashSet<Genre>();
        }
    }
}
