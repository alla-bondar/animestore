﻿CREATE TABLE [dbo].[Products] (
    [ProductId]         INT             IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (100)  NOT NULL,
    [Year]              INT             NOT NULL,
    [NumOfDiscsInComplect]        INT             DEFAULT ((1)) NOT NULL,
    [Price]             DECIMAL (16, 2) NOT NULL,
    [StockAvailability] BIT             DEFAULT ((1)) NOT NULL,
    [NumOfSeries]       INT             NOT NULL,
    [Director]          NVARCHAR (50)   NOT NULL,
    [Scriptwriter]      NVARCHAR (50)   NOT NULL,
    [Description]       NVARCHAR (2000) NOT NULL,
    [Category]          CHAR (1)        DEFAULT ('K') NOT NULL,
    [Image]             NVARCHAR (100)  NULL,
    PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

