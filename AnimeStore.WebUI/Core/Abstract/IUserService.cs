﻿using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Models;

namespace AnimeStore.WebUI.Core.Abstract
{
    public interface IUserService
    {
        User GetCurrentUser();

        bool IsUserWithLoginExists(string login);

        bool IsUserWithEmailExists(string email);

        CreateUserModel CreateUser(RegistrationModel model);

        bool LoginUser(string login, string password, bool rememberMe = false);

        bool IsAuthenticated();

        void LogoutUser();
    }
}
