﻿using System;
using AnimeStore.WebUI.Entities;
namespace AnimeStore.WebUI.Infrastructure.Helpers
{
    public static class ImageHelpers
    {
        public static string GetImageUrl(this string imageName)
        {
            return String.Concat(Constants.ImagesPath, imageName);
        }

        public static string GetProductImageUrl(this string imageName)
        {
            return String.Concat(Constants.ProductsImagesPath, imageName);
        }

        public static string GetProductMark(this string productName)
        {
            return String.Concat(Constants.PageMarker, productName);
        }

        public static string GetSavePathForProductImage(this string imageName)
        {
            return String.Concat("~", Constants.ProductsImagesPath, imageName);
        }
    }
}