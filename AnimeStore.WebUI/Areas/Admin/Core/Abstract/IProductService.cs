﻿namespace AnimeStore.WebUI.Areas.Admin.Core.Abstract
{
    public interface IProductService
    {
        void SaveProductTypeAndGenre(int productId, int[] typeIds, int[] genreIds);
        void DeleteProductTypes(int productId);
        void DeleteProductGenres(int productId);
    }
}
