﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;
using AnimeStore.Domain.Abstract;

namespace AnimeStore.Domain.Concrete
{
    public class EFUserRepository : IUserRepository
    {
        private readonly EFDbContext context;

        public EFUserRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<User> Users
        {
            get { return context.Users; }
        }

        public void SaveUser(User user)
        {
            if (user.UserId == 0)
                context.Users.Add(user);
            else
            {
                User dbEntry = context.Users.Find(user.UserId);
                if (dbEntry != null)
                {
                    dbEntry.Login = user.Login;
                    dbEntry.Email = user.Email;
                    dbEntry.FirstName = user.FirstName;
                    dbEntry.LastName = user.LastName;
                }
            }
            context.SaveChanges();
        }
    }
}
