﻿using System;

namespace AnimeStore.WebUI.Infrastructure.Helpers
{
    public static class StringHelper
    {
        public static bool ContainsIgnoreCase(this string source,string value )
        {
            return source.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}