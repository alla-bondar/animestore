﻿using AnimeStore.Domain.Entities;
namespace AnimeStore.WebUI.Models
{
    public class ChosenOrderItemModel
    {
        public Product Item;
        public int Count;
    }
}