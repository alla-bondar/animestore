﻿using System.Collections.Generic;
namespace AnimeStore.WebUI.Models
{
    public class OrderHistoryModel
    {
        public int Days { get; set; }
        public List<OrderModel> Orders { get; set; }

        public OrderHistoryModel()
        {
            Orders = new List<OrderModel>();
        }
    }
}