﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Abstract
{
    public interface IUserRepository
    {
        IEnumerable<User> Users { get; }

        void SaveUser(User user);
    }
}
