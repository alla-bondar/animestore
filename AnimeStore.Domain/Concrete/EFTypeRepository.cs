﻿using System.Collections.Generic;
using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Concrete
{
    public class EFTypeRepository : ITypeRepository
    {
        private readonly EFDbContext context;

        public EFTypeRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Type> Types
        {
            get { return context.Types; }
        }
    }
}
