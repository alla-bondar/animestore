﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.WebUI.Models
{
    public class OrderModel
    {
        public Order Order { get; set; }
        public List<OrderItemInfoModel> OrderItemsInfo;

        public OrderModel()
        {
            OrderItemsInfo = new List<OrderItemInfoModel>();
        }
    }
}