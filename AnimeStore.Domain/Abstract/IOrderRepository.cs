﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;

namespace AnimeStore.Domain.Abstract
{
    public interface IOrderRepository
    {
        IEnumerable<Order> Orders { get; }

        int SaveOrder(Order order);
    }
}
