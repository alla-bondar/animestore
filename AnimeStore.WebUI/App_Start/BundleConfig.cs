﻿using System.Web.Optimization;

namespace AnimeStore.WebUI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/myscripts").Include(
                        "~/Scripts/MyScripts/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/libs/jquery-2.2.0.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/*.css"));

            bundles.Add(new StyleBundle("~/Admin/Content/css").Include(
                        "~/Areas/Admin/Content/*.css",
                        "~/Content/errorStyles.css",
                        "~/Content/jquery.multiselect.css"));

            bundles.Add(new ScriptBundle("~/bundles/Admin/js").Include(
                        "~/Scripts/libs/jquery.multiselect.js",
                        "~/Areas/Admin/Scripts/*.js"));
        }
    }
}