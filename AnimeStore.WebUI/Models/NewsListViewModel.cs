﻿using AnimeStore.Domain.Entities;
using System.Collections.Generic;

namespace AnimeStore.WebUI.Models
{
    public class NewsListViewModel
    {
        public IEnumerable<News> News { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}