﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace AnimeStore.Domain.Entities
{
    [Table("News")]
    public class News
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [HiddenInput(DisplayValue = false)]
        public int NewsId { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DateOfNews { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите автора")]
        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите заголовок новости")]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Пожалуйста, напишите описание новости")]
        [Display(Name = "Описание")]
        [DataType(DataType.MultilineText)]
        public string Preview { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Пожалуйста, напишите текст новости")]
        [Display(Name = "Текст новости")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        [Required(ErrorMessage = "Пожалуйста, напишите категорию новости")]
        [Display(Name = "Категория")]
        [DataType(DataType.MultilineText)]
        public string Category { get; set; }
    }
}
