﻿using AnimeStore.Domain.Abstract;
using AnimeStore.Domain.Entities;
using AnimeStore.WebUI.Areas.Admin.Models;
using AnimeStore.WebUI.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace AnimeStore.WebUI.Controllers
{
    public class NewsController : Controller
    {
        private readonly INewsRepository newsRepository;
        public const int PageSize = 3;

        public NewsController(INewsRepository newsRepository)
        {
            this.newsRepository = newsRepository;
        }

        public ActionResult Index(int page = 1)
        {
            NewsListViewModel model = new NewsListViewModel
            {
                News = newsRepository.News
                                   .OrderByDescending(news => news.NewsId)
                                   .Skip((page - 1) * PageSize)
                                   .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    TotalPages = (int)Math.Ceiling((double)newsRepository.News.Count() / PageSize)
                }
            };
            return View(model);
        }

        public ActionResult Info(int id)
        {
            string returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer != null ? System.Web.HttpContext.Current.Request.UrlReferrer.ToString() : Url.Action("Index");

            News news = newsRepository.News.First(n => n.NewsId == id);
            if (news == null)
            {
                return Redirect(returnUrl);
            }

            EditNewsViewModel model = new EditNewsViewModel();
            model.News = news;
            model.ReturnUrl = returnUrl;

            return View(model);
        }
    }
}