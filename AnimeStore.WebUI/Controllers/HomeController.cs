﻿using System.Web.Mvc;

namespace AnimeStore.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }       
    }
}
