﻿namespace AnimeStore.WebUI.Models
{
    public class CreateUserModel
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }
}