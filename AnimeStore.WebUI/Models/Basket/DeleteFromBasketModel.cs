﻿namespace AnimeStore.WebUI.Models.Basket
{
    public class DeleteFromBasketModel
    {
        public int OrderItemId { get; set; }
    }
}
