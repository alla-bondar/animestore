﻿using System.Web.Mvc;
using AnimeStore.WebUI.Models;
using System.Text.RegularExpressions;
using AnimeStore.WebUI.Filters;
using AnimeStore.WebUI.Core.Abstract;

namespace AnimeStore.WebUI.Controllers
{
    [Authorize]
    public class AuthenticationController : Controller
    {
        private readonly IBasketService basketService;
        private readonly IUserService userService;

        public AuthenticationController(IBasketService basketService, IUserService userService)
        {
            this.basketService = basketService;
            this.userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        [ForNotAuthorize]
        public ActionResult Registration()
        {
            RegistrationModel user = new RegistrationModel();
            return View(user);
        }

        [HttpPost]
        [AllowAnonymous]
        [ForNotAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                if (userService.IsUserWithLoginExists(model.Login))
                {
                    ModelState.AddModelError("NotUniqueUserError",
                        "Пользователь с таким логином уже зарегистрирован!");
                }
                Regex regex = new Regex("^[a-zA-Z0-9]*$");
                if (!regex.IsMatch(model.Login))
                {
                    ModelState.AddModelError("Login",
                        "Логин должен состоять только с цифр и латинских букв");
                }
                if (userService.IsUserWithEmailExists(model.Email))
                {
                    ModelState.AddModelError("NotUniqueUserError",
                        "Пользователь с таким email уже зарегистрирован!");
                }
                regex = new Regex("[0-9]+");
                if (regex.IsMatch(model.FirstName))
                {
                    ModelState.AddModelError("FirstName",
                        "В имени не должно быть цифр");
                }
                if (regex.IsMatch(model.LastName))
                {
                    ModelState.AddModelError("LastName",
                        "В фамилии не должно быть цифр");
                }
                if (ModelState.IsValid)
                {
                    CreateUserModel createUserModel = userService.CreateUser(model);
                    if (createUserModel.IsSuccess)
                    {
                        userService.LoginUser(model.Login, model.Password);
                        return RedirectToAction("Index", "Home");
                    }

                    ModelState.AddModelError("NotUniqueUserError", createUserModel.ErrorMessage);
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SignIn()
        {
            
            LoginModel userModel = new LoginModel();
            return View(userModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(LoginModel model)
        {
            if (ModelState.IsValid && userService.LoginUser(model.Login,
                    model.Password, model.RememberMe))
            {
                return RedirectToAction("Index", "Home");
            }
            
            ModelState.AddModelError("modelError",
                        "Неверный логин или пароль");

            return View("SignIn");
        }

        public ActionResult SignOut()
        {
            userService.LogoutUser();
            basketService.ClearCart();
            return RedirectToAction("Index", "Home");
        }
    }
}
