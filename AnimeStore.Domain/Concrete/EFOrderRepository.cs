﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;
using AnimeStore.Domain.Abstract;

namespace AnimeStore.Domain.Concrete
{
    public class EFOrderRepository : IOrderRepository
    {
        private readonly EFDbContext context;

        public EFOrderRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Order> Orders
        {
            get { return context.Orders; }
        }

        public int SaveOrder(Order order)
        {
            if (order.OrderId == 0)
            {
                order.DateOfStartOrder = System.DateTime.Now;
                order.OrderId = context.Orders.Add(order).OrderId;
            }
            else
            {
                Order dbEntry = context.Orders.Find(order.OrderId);
                if (dbEntry != null)
                {
                    dbEntry.User = order.User;
                    dbEntry.TotalPrice = order.TotalPrice;
                    dbEntry.IsConfirmed = order.IsConfirmed;
                    dbEntry.DateOfOrder = order.DateOfOrder;
                }
            }
            context.SaveChanges();
            return order.OrderId;
        }
    }
}
