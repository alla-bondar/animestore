﻿using AnimeStore.Domain.Entities;
using System.Data.Entity;

namespace AnimeStore.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base("EFDbContext")
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Type> Types { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Type>()
                .HasMany(x => x.Products)
                .WithMany(x => x.Types)
            .Map(x =>
            {
                x.ToTable("TypeProducts");
                x.MapLeftKey("TypeId");
                x.MapRightKey("ProductId");
            });

            modelBuilder.Entity<Genre>()
                .HasMany(x => x.Products)
                .WithMany(x => x.Genres)
            .Map(x =>
            {
                x.ToTable("GenreProducts");
                x.MapLeftKey("GenreId");
                x.MapRightKey("ProductId");
            });
        }
    }
}
