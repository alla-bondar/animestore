﻿namespace AnimeStore.WebUI.Models
{
    public class SearchResultModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Preview { get; set; }
        public string Image { get; set; }
    }
}