﻿using System.Collections.Generic;
using AnimeStore.Domain.Entities;
using AnimeStore.Domain.Abstract;

namespace AnimeStore.Domain.Concrete
{
    public class EFOrderItemRepository : IOrderItemRepository
    {
        private readonly EFDbContext context;

        public EFOrderItemRepository(EFDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<OrderItem> OrderItems
        {
            get { return context.OrderItems; }
        }

        public void SaveOrderItem(OrderItem orderItem)
        {
            if (orderItem.OrderItemId == 0)
                context.OrderItems.Add(orderItem);
            else
            {
                OrderItem dbEntry = context.OrderItems.Find(orderItem.OrderItemId);
                if (dbEntry != null)
                {
                    dbEntry.OrderId = orderItem.OrderId;
                    dbEntry.ProductId = orderItem.ProductId;
                    dbEntry.NumOfComplects = orderItem.NumOfComplects;
                    dbEntry.PriceForComplect = orderItem.PriceForComplect;
                }
            }
            context.SaveChanges();
        }

        public void DeleteItem(int orderItemId)
        {
            OrderItem dbEntry = context.OrderItems.Find(orderItemId);
            if (dbEntry != null)
            {
                context.OrderItems.Remove(dbEntry);
                context.SaveChanges();
            }
        }
    }
}
